(function() {
  'use strict';

  angular
    .module('app.userBooksList')
    .config(configFunction);

  configFunction.$inject = ['$routeProvider'];

  function configFunction($routeProvider) {
    $routeProvider.when('/userbookslist', {
      templateUrl: 'app/UserBooksList/UserBooksList.html',
      controller: 'UserBooksListController',
      controllerAs: 'vm',
      resolve: { user: resolveUser }
    });
  }

  resolveUser.$inject = ['$auth', '$location'];

  function resolveUser($auth, $location) {
    $auth.validateUser().catch(function(response) {
      $location.path('/');
    });
  }

})();

(function () {
    'use strict';

    angular
        .module('app.userBooksList')
        .directive('gzBooksForm', gzBooksForm);

    function gzBooksForm() {
        return {
            templateUrl: 'app/userBooksList/directives/booksForm.html',
            restrict: 'E',
            controller: BooksFormController,
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                offers: '='
            }
        }
    }

    BooksFormController.$inject = ['offerService', '$auth'];

    function BooksFormController(offerService, $auth) {
        var vm = this;

        vm.newOffer = new offerService.offer();
        vm.addOffer = addOffer;
        vm.autofill = autofill;
        vm.autofillData = null;
        vm.clear = clear;
        vm.alerts = [];

        function addOffer() {
            offerService.saveOffer(vm.newOffer).then(function (response) {
                offerService.getOffersByCurrentUser().then(function (response) {
                    vm.offers = response;
                    vm.closeAlert();
                    vm.newOffer = offerService.offer();
                });
            }).catch(function (response) {
                vm.autofillData = response.data;
                vm.addAlert();
            });
        }

        function autofill() {
          vm.newOffer = vm.autofillData;

            vm.alerts = [];
            vm.alerts.push({msg: "Brawo! Kliknij 'Dodaj' aby dodać książkę do listy."})
        }

        function clear() { vm.newOffer = [];
        vm.alerts = [];}

        vm.addAlert = function () {
            vm.alerts = [];
            vm.alerts.push({msg: "Pozycja o danym numerze ISBN znajduje się już w bazie. Kliknij 'Uzupełnij', aby automatycznie uzupełnić dane książki. Kliknij 'wyczyść', aby wyczyścić zawartość formularza."});
        };

        vm.closeAlert = function () {
            vm.alerts = [];
        };
    }
})();

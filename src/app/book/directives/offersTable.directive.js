(function() {
  'use strict';

  angular
    .module('app.book')
    .directive('gzOffersTable', gzOffersTable);

  function gzOffersTable() {
    return {
      templateUrl: 'app/book/directives/offersTable.html',
      restrict: 'E',
      controller: OffersTableController,
      controllerAs: 'vm',
      bindToController: true,
      scope: {
        offers: '=',
        book: '='
      }
    }
  }

  OffersTableController.$inject = ['messageService'];

  function OffersTableController(messageService) {
    var vm = this;

    vm.newMessage = new messageService.newMessage();
    vm.sendMessage = sendMessage;
    vm.modalAlerts = [];
    vm.messageAlerts = [];

    $('.message-modal').on('shown.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      $('.message-modal-receiver-email').html(button.data('receiver-email')).change();
      $('.message-modal-receiver-id').val(button.data('receiver-id')).change();
      $('.message-modal-subject').val(button.data('book-title')).change();
    });

    function sendMessage() {
      messageService.sendMessage(vm.newMessage).then(function(response) {
        $('.message-modal').modal('toggle');
        vm.messageAlerts = [];
        vm.messageAlerts.push({ msg: response })
      }).catch(function(response) {
        vm.modalAlerts = [];
        $.each(response.data, (function (index, value) {
          console.log(index);
          vm.modalAlerts.push({ msg: index + ' ' + value });
        }));
      });
    }
  }
})();

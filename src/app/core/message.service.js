(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('messageService', messageService);

  messageService.$inject = ['API_URL', '$http'];

  function messageService(API_URL, $http) {
    var service = {
      sendMessage: sendMessage,
      newMessage: newMessage
    };

    return service;

    ////////////

    function newMessage() {
      this.receiver_id = '';
      this.subject = '';
      this.message = '';
    }

    function sendMessage(message) {
      return $http({
        method: 'POST',
        url: API_URL + 'messages',
        params: message
      }).then(function successCallback(response) {
        return response.data
      });
    }
  }

})();

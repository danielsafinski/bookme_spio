exports.config = {
    // framework: 'custom',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./tests/*.js'],

    capabilities: {
        'browserName': 'chrome'
    },

     // baseUrl: 'http://localhost:8080',

    onPrepare: function () {
        //browser.ignoreSynchronization = true;
        browser.driver.manage().window().maximize();
    },
    // framework: 'jasmine',
    // jasmineNodeOpts: {
    //     showColors: true
    // }
    // jasmineNodeOpts: {
    //     showColors: true // Use colors in the command line report.
    // }

};
(function() {
    'use strict';

    angular
        .module('app.userBooksList')
        .controller('UserBooksListController', UserBooksListController);

    UserBooksListController.$inject = ['$rootScope', 'offerService', 'user'];

    function UserBooksListController($rootScope, offerService, user) {
        var vm = this;

        offerService.getOffersByCurrentUser().then(function(response) {
            vm.offers = response;
        });
    }

})();

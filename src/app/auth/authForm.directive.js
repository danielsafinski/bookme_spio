(function () {
    'use strict';

    angular
        .module('app.auth')
        .directive('gzAuthForm', gzAuthForm);

    function gzAuthForm() {
        return {
            templateUrl: 'app/auth/authForm.html',
            restrict: 'E',
            controller: AuthFormController,
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                submitAction: '&',
                error: '=',
                formTitle: '@',
                command: '@'
            },
            transclude: true
        }
    }

    AuthFormController.$inject = ['authService'];

    function AuthFormController(authService) {
        var vm = this;

        vm.user = {
            email: '',
            password: ''
        };

        function isLoggedIn() {
            return authService.isLoggedIn();
        }
    }

})();
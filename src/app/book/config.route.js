(function() {
  'use strict';

  angular
    .module('app.book')
    .config(configFunction);

  configFunction.$inject = ['$routeProvider'];

  function configFunction($routeProvider) {
    $routeProvider.when('/books/:id', {
      templateUrl: 'app/book/book.html',
      controller: 'BookController',
      controllerAs: 'vm',
      resolve: { user: resolveUser }
    });

    resolveUser.$inject = ['$auth', '$location'];

    function resolveUser($auth, $location) {
      $auth.validateUser().catch(function(response) {
        $location.path('/');
      });
    }
  }
})();

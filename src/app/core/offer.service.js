(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('offerService', offerService);

  offerService.$inject = ['API_URL', '$http'];

  function offerService(API_URL, $http) {
    var service = {
      getOffersByCurrentUser: getOffersByCurrentUser,
      saveOffer: saveOffer,
      updateOffer: updateOffer,
      deleteOffer: deleteOffer,
      offer: offer,
    };

    return service;

    ////////////

    function getOffersByCurrentUser() {
      return $http({
        method: 'GET',
        url: API_URL + 'offers'
      }).then(function successCallback(response) {
        return response.data
      });
    }

    function saveOffer(offer) {
      return $http({
        method: 'POST',
        url: API_URL + 'offers',
        params: offer
      }).then(function successCallback(response) {
        return response.data
      });
    }

    function updateOffer(offer_id) {
      return $http({
        method: 'PUT',
        url: API_URL + 'offers/' + offer_id
      }).then(function successCallback(response) {
        return response.data
      });
    }

    function deleteOffer(offer_id) {
      return $http({
        method: 'DELETE',
        url: API_URL + 'offers/' + offer_id
      }).then(function successCallback(response) {
        return response.data
      });
    }

    function offer() {
      this.author = '';
      this.title = '';
      this.isbn = '';
      this.publisher = '';
      this.genre = '';
    }
  }

})();
